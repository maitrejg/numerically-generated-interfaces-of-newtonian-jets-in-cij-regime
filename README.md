# Numerically-generated interfaces of Newtonian jets in CIJ regime

Minimal working examples (MWE) using data from [1], in a python notebook.

Note that in these examples only one of the 1081 Reynolds-Amplitude pairs  is used.

## Example 1 

Loading and plotting the jet with the shortest $drop~~0$, i.e. with the minimal breakup length using both matplotlib ang gnuplot.

## Example 2 

Plotting a sequence of $n$ jets on the same figure.

## Bibliography 

[1] Maitrejean, Guillaume (2022), “Numerically-generated interfaces of Newtonian jets in CIJ regime”, Mendeley Data, V1, doi: 10.17632/3ds9h73pnv.1